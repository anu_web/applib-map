import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  GoogleMap, Marker, Polygon, Polyline, withGoogleMap,
} from 'react-google-maps';

import {
  defaultPropsMapElements, propTypesMapElements, withMapView,
} from '..';


const defaultMarkerSize = 38;
const defaultMarker = {
  path: 'm -0.5,0  a 0.5,0.5 0 1,0 1,0  a 0.5,0.5 0 1,0 -1,0',
  fillColor: '#ea4334',
  fillOpacity: 1,
  strokeWeight: 1,
};
const zIndexPolygon = 10;
const zIndexPolyline = 11;
const zIndexMarker = 12;
const zIndexActiveMarker = 19;
const zIndexActiveIndicator = 20;

class GoogleMapElements extends Component {
  static propTypes = propTypesMapElements;

  static defaultProps = defaultPropsMapElements;

  onClick = (event, key, { geo_type: geoType, lat, lng }) => {
    const { onSelectElement } = this.props;
    if (geoType === 'Point') {
      // Use point location as selected position.
      const position = { lat, lng };
      onSelectElement({ key, options: { position } });
    } else if (geoType === 'Polygon' || geoType === 'Line') {
      // Use click location as selected position.
      const position = event.latLng;
      onSelectElement({ key, options: { position } });
    }
  };

  render() {
    const {
      mapElements,
      selection,
      mapElementOptions,
      typeOptions,
      onMapRender,
      ...restProps
    } = this.props;
    const { key: selected, options: selectionOptions } = selection || {};
    const { position: selectedPosition } = selectionOptions || {};

    const selectedMarker = mapElements[selected];
    const activePosition = selectedPosition || (
      selectedMarker && { lat: selectedMarker.lat, lng: selectedMarker.lng }
    );

    return (
      <GoogleMap ref={onMapRender || null} {...restProps}>
        {selected && activePosition && (
          <Marker
            key="activeIndicator"
            position={activePosition}
            zIndex={zIndexActiveIndicator}
          />
        )}

        {Object.entries(mapElements).map(([key, element]) => {
          const {
            geo_type: geoType, type, lat, lng, path_data: pathData,
          } = element;
          const onClick = event => this.onClick(event, key, element);
          const {
            icon_url: iconUrl,
            stroke_width: strokeWeight,
            stroke_color: strokeColor,
            fill_color: fillColor,
          } = typeOptions[type] || {};

          if (geoType === 'Polygon' && pathData) {
            const options = {
              strokeWeight,
              strokeColor,
              fillColor,
              zIndex: zIndexPolygon,
            };
            return (
              <Polygon
                key={key}
                paths={pathData}
                options={options}
                onClick={onClick}
              />
            );
          }

          if (geoType === 'Line' && pathData) {
            const options = {
              strokeWeight,
              strokeColor,
              zIndex: zIndexPolyline,
            };

            return pathData.map((path, i) => (
              <Polyline
                key={`${key}-${i}` /* eslint-disable-line react/no-array-index-key */}
                path={path}
                options={options}
                onClick={onClick}
              />
            ));
          }

          if (geoType === 'Point' && lat && lng) {
            const markerSize = mapElementOptions.markerSize || defaultMarkerSize;
            const icon = iconUrl
              ? {
                url: iconUrl,
                scaledSize: { width: markerSize, height: markerSize },
                anchor: { x: markerSize / 2, y: markerSize / 2 },
              }
              : {
                ...defaultMarker,
                scale: markerSize,
              };

            const options = {
              zIndex: selected === key ? zIndexActiveMarker : zIndexMarker,
            };

            return (
              <Marker
                key={key}
                position={{ lat, lng }}
                icon={icon}
                options={options}
                onClick={onClick}
              />
            );
          }

          // eslint-disable-next-line no-console
          console.warn(`Map element '${key}' is not renderable.`);
          return null;
        })}
      </GoogleMap>
    );
  }
}

export const WebMapView = withMapView(withGoogleMap(GoogleMapElements));
