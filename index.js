import React, { Component } from 'react';
import PropTypes from 'prop-types';
import filter from 'object-loops/filter';


const withTypeFilters = (Elements) => {
  const FilteredElements = ({ mapElements, typeFilters, ...restProps }) => {
    const filtered = filter(mapElements, element => (
      typeFilters === null || typeFilters.includes(element.type)
    ));

    return <Elements mapElements={filtered} {...restProps} />;
  };
  FilteredElements.propTypes = {
    mapElements: PropTypes.objectOf(PropTypes.shape({
      type: PropTypes.string.isRequired,
    })).isRequired,
    typeFilters: PropTypes.arrayOf(PropTypes.string),
  };
  FilteredElements.defaultProps = {
    typeFilters: null,
  };

  return FilteredElements;
};


const withSelection = Elements => (
  class extends Component {
    static propTypes = {
      mapElements: PropTypes.objectOf(PropTypes.object).isRequired,
      selected: (props, propName) => { // eslint-disable-line consistent-return
        const elementKey = props[propName];
        if (props[propName] !== null && !(elementKey in props.mapElements)) {
          return new Error(`Selected element '${elementKey}' is not found in map elements.`);
        }
      },
      onSelect: PropTypes.func,
    };

    static defaultProps = {
      selected: null,
      onSelect: null,
    };

    constructor(props) {
      super(props);

      this.lastSelection = null;
    }

    onSelectElement = (selection) => {
      this.lastSelection = selection;

      const { onSelect } = this.props;
      if (onSelect) {
        const { key } = selection;
        onSelect(key);
      }
    };

    render() {
      const { selected, ...restProps } = this.props;
      const defaultSelection = selected ? { key: selected } : null;
      const selection = this.lastSelection && this.lastSelection.key === selected
        ? this.lastSelection : defaultSelection;

      return (
        <Elements
          selection={selection}
          onSelectElement={this.onSelectElement}
          {...restProps}
        />
      );
    }
  }
);


export const withMapView = Elements => (
  withTypeFilters(withSelection(Elements))
);


const propTypePosition = {
  lat: PropTypes.number,
  lng: PropTypes.number,
};

export const propTypesMapElements = {
  mapElements: PropTypes.objectOf(PropTypes.shape({
    geo_type: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    ...propTypePosition,
    path_data: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape(propTypePosition))),
  })).isRequired,
  selection: PropTypes.shape({
    key: PropTypes.string.isRequired,
    options: PropTypes.object,
  }),
  onMapRender: PropTypes.func,
  onSelectElement: PropTypes.func,
  mapElementOptions: PropTypes.objectOf(PropTypes.shape({
    markerSize: PropTypes.number,
    markerOriginalSize: PropTypes.number,
  })),
  typeOptions: PropTypes.objectOf(PropTypes.object),
};


export const defaultPropsMapElements = {
  selection: null,
  onMapRender: null,
  onSelectElement: null,
  mapElementOptions: {},
  typeOptions: {},
};
